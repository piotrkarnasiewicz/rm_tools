import os
from json import load
from sys import argv

from com.functions import check, create_branches, create_tags, cleanup, create_pull_requests
from com.logger import Logger


def run():
    # Get the version number
    global version
    if len(argv) > 1:
        version = argv[1]
    else:
        version = read_version()

    # Get repositories data
    repos = load(open("repos.json"))

    # Get the home dir
    home_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/..")

    command = ''
    while command.lower() != 'q':
        Logger.underline("\n1) Check repos   2) Create branches   3) Change version  4) Create tags  5) Cleanup "
                         + "6) Create pull requests  Q) Exit")
        command = input()[0]

        if command == "1":
            check(version, repos, home_dir)
        elif command == "2":
            create_branches(version, repos, home_dir)
        elif command == "3":
            version = read_version()
        elif command == "4":
            create_tags(version, repos, home_dir)
        elif command == "5":
            cleanup(version, repos, home_dir)
        elif command == "6":
            dest = input("Select destination branch: ")
            create_pull_requests(version, repos, home_dir, dest)


def read_version():
    return input("Insert version: ")


# curl --request DELETE --header "PRIVATE-TOKEN: 6iDoxLisJA7Efx98ozB5"
# 'https://gitlab.workfront.tech/api/v4/projects/714/repository/branches/patch%2Fpiotr_to_test2'
if __name__ == '__main__':
    run()
