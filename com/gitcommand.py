import shlex
import subprocess


class GitCommand:
    @staticmethod
    def __run(command):
        # pprint(shlex.split(command, posix=False))
        subprocess.run(shlex.split(command, posix=False))

    @staticmethod
    def branch_create(branch):
        GitCommand.__run("git checkout -b " + branch)
        GitCommand.__run("git push origin -u " + branch)

    @staticmethod
    def branch_delete_local(branch):
        GitCommand.__run("git branch {} -D".format(branch))

    @staticmethod
    def branch_exists(branch):
        cp = subprocess.run(("git branch --remote --list origin/" + branch).split(" "), stdout=subprocess.PIPE)
        return len(cp.stdout) > 0

    @staticmethod
    def branch_list(pattern):
        GitCommand.__run("git branch --remote --list " + pattern)

    @staticmethod
    def checkout(branch):
        GitCommand.__run("git checkout " + branch + " -q")

    @staticmethod
    def clone(url):
        GitCommand.__run("git clone " + url)

    @staticmethod
    def compare(branch1, branch2):
        GitCommand.__run("git rev-list --left-right --count " + branch1 + "..." + branch2)

    @staticmethod
    def fetch():
        GitCommand.__run("git fetch -a -p -q")

    @staticmethod
    def pull():
        GitCommand.__run("git pull -r -q")

    @staticmethod
    def pull_tags():
        GitCommand.__run("git pull -r --tags -q")

    @staticmethod
    def rebase():
        GitCommand.__run("git rebase -q")

    @staticmethod
    def tag(tag, message):
        GitCommand.__run("git tag -a {} -m \"{}\"".format(tag, message))
        GitCommand.__run("git push --tags")

    @staticmethod
    def tag_exists(tag):
        cp = subprocess.run(("git tag -l " + tag).split(" "), stdout=subprocess.PIPE)
        return len(cp.stdout) > 0
