class Logger:
    """
    Formats output using custom colors.
    """

    BLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    ERROR = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    @staticmethod
    def log(message, color):
        """
        Prints message to STDOUT using color given.
        :param message:
        :param color:
        :return:
        """
        print(color + message + Logger.ENDC)

    @staticmethod
    def info(message):
        Logger.log(message, Logger.BLUE)

    @staticmethod
    def warn(message):
        Logger.log(message, Logger.WARNING)

    @staticmethod
    def success(message):
        Logger.log(message, Logger.GREEN)

    @staticmethod
    def error(message):
        Logger.log(message, Logger.ERROR)

    @staticmethod
    def text(message):
        print(message)

    @staticmethod
    def underline(message):
        Logger.log(message, Logger.UNDERLINE)
