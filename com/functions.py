import webbrowser
from os import chdir, mkdir
from os.path import isdir

from com.gitcommand import GitCommand
from com.logger import Logger

REPO_URL = "git@gitlab.workfront.tech:{}/{}.git"


def get_branch_name(version):
    if version[0] == 'I' or version[0] == 'R' or version.endswith(".0.0"):
        return "release/" + version
    elif version.endswith(".0"):
        return "patch/" + version
    else:
        return "hotfix/" + version


def check(version, repos, home_dir):
    """
    Checks git repository if the version exist
    :param version:
    :param repos:
    :param home_dir:
    :return:
    """
    found = []
    for repo_name, repo in repos.items():
        Logger.info("Checking " + repo_name)
        chdir(home_dir)
        if not isdir(repo_name):
            mkdir(repo_name)
            Logger.warn("Repo {} doesn't exist, cloning".format(repo_name))
            GitCommand.clone(REPO_URL.format(repo["project"], repo_name))
        chdir(repo_name)
        GitCommand.fetch()
        GitCommand.checkout("master")
        GitCommand.pull_tags()
        GitCommand.checkout("dev")
        GitCommand.pull()

        Logger.info("Existing patch and hotfix branches:")
        GitCommand.branch_list("origin/patch/*")
        GitCommand.branch_list("origin/hotfix/*")

        Logger.info("Number of new commits comparing master and dev:")
        GitCommand.compare("origin/master", "origin/dev")

        branch = get_branch_name(version)

        if GitCommand.branch_exists(branch):
            found.append(repo_name)
            Logger.info("Number of new commits comparing master and {}:".format(branch))
            GitCommand.compare("origin/master", "origin/" + branch)
        else:
            Logger.warn("Remote branch {} doesn't exist in {}".format(branch, repo_name))
        print("")

    if len(found) > 0:
        Logger.success("Version found in repos: {}".format(", ".join(found)))
    else:
        Logger.error("Version not found in any repository")


def create_branches(version, repos, home_dir):
    tag = "v{}.RC".format(version)

    for repo_name, repo in repos.items():
        Logger.info("Creating branch for " + repo_name)
        chdir(home_dir)
        if isdir(repo_name):
            chdir(repo_name)
            GitCommand.fetch()
            if not GitCommand.tag_exists(tag):
                GitCommand.checkout("dev")
                GitCommand.rebase()
                GitCommand.tag(tag, "Release candidate: " + tag)
            GitCommand.checkout(tag)
            GitCommand.branch_create("release/" + version)
            GitCommand.tag(tag + ".1", "Version " + tag + ".1")
            Logger.success("OK")
        else:
            Logger.error("Repo {} doesn't exist".format(repo_name))
        print("")


def create_tags(version, repos, home_dir):
    tag = "v" + version
    branch = get_branch_name(version)
    for repo_name, repo in repos.items():
        Logger.info("Tagging " + repo_name)
        chdir(home_dir)
        if isdir(repo_name):
            chdir(repo_name)
            GitCommand.fetch()
            if GitCommand.branch_exists(branch):
                GitCommand.checkout(branch)
                GitCommand.rebase()
                GitCommand.tag(tag, "Version: " + tag)
                Logger.success("Repo {} tagged.".format(repo_name))
        else:
            Logger.error(repo_name + "doesn't exist")
        print("")


def cleanup(version, repos, home_dir):
    branch = get_branch_name(version)
    for repo_name, repo in repos.items():
        Logger.info("Deleting {} in {}".format(branch, repo_name))
        chdir(home_dir)
        if isdir(repo_name):
            chdir(repo_name)
            GitCommand.fetch()
            GitCommand.checkout("dev")
            GitCommand.rebase()
            GitCommand.branch_delete_local(branch)
        else:
            Logger.error(repo_name + "doesn't exist")
        print("")


def create_pull_requests(version, repos, home_dir, dest):
    src = get_branch_name(version)
    for repo_name, repo in repos.items():
        chdir(home_dir + "/" + repo_name)
        Logger.info("Checking " + repo_name)
        GitCommand.fetch()
        if GitCommand.branch_exists(src):
            url = "https://bitbucket.org/workfrontinc/"
            url += "{0}/pull-requests/new?source=workfrontinc/{0}::{1}&dest=workfrontinc/{0}::{2}".format(
                repo_name, src, dest)
            webbrowser.open_new_tab(url)
        print("")
